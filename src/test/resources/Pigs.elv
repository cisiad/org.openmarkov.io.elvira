// Bayesian Network
//   Elvira format 

bnet  Pig_Network { 

// Network Properties

version = 1.0;
default node states = (absent , present);

// Network Variables 

node p630400490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48124091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627270088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392115290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627276488(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392150190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48109691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630071089(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630067789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630384190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48109791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83306289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83456290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p277195691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p277114088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197132888(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p277155690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p277195791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p277111088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p277162190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p216124491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230416387(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p216077190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p216124591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630396290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630182291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630031389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630299990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82019685(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p803043885(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751512889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392157391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48147992(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630439091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48148092(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630388390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83567891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83314589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83470790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48084891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630014189(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630323790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630155091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543072191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543472088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543654190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543072291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547629489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547097990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197119188(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197140688(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p753023491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p609183992(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543036891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547633289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547620489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547054590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543036991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543378087(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197125588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543517389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543657690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543084792(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630430091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48127091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50261091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630398790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48111891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48172392(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630390690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48172492(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50241090(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82154988(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82236090(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p750365488(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392120790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630328490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630152091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630328890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630772188(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630282090(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48112891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50052788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630672087(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50168489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48128191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48139191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630426691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48139291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83572791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83371889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83324189(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83474091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83572891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543068091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543068391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392203792(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522398991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543419788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522334189(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522204687(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522279888(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522369890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522449292(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197240391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197143789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197192390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197240491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197258291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197258391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197258591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197303091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197303191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197162589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82191389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197201290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197303291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197288691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197149689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197318792(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197210690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197165689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197206590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197075886(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751230786(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197126088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197229090(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197288791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197276591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197180690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197252391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197252591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197131388(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197206490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197256791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197353592(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50139889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50195390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48004891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630172991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630173091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630147391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630147591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630154689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197130688(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630154989(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630810388(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630067989(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197263491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197130288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197153289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197235390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751038090(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p88127791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751513589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p88067190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392012788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197168789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p88014589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197211690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197314191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50095388(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48022391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630810288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50212890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630311090(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48022291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230057992(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230433487(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p603176686(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230565789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230757791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82065086(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392087690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82349391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82345291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82258290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82206789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82210989(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82258390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82349491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48000191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48000291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48110491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48110591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630088089(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630384990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630813788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630062489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630040991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630154291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630052589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630239990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630475686(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630058889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630643987(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630652587(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630073589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630266190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50241490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48072391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630373290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630815588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630810088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630068889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630373890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48197592(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48213092(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83290288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630062389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630064189(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83403790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630249690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p83500691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48213192(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48000691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82243490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82197489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82198489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82243390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82292291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82241490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82050786(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522082985(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82218889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82347891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82338291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82338391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82206489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p826030789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82307191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82422492(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48064091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48064191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82142888(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82247790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82152588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82238890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82334391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751015990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48064391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50197190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630258690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48013791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50197390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50197590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50133689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50140189(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50197290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48007991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82228290(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82105287(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82228490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82277591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82182889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82218289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543151292(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751106191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543151392(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543709391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543420288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543583789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82183689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547497788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82266890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82332291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82261490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82303591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82141488(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197130888(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82303691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82211489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82206389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82261890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82366892(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82185289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82191289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82229690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82280791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82224990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82225190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441290591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441324091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441324191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441231890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441119488(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441231590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441290691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441118988(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441214590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p609091487(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p609134289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82133387(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82225690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82350491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p958249187(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p95141490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p95247691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p95090289(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p95147490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627412591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627396491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627343790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627275788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627294789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627344390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627412991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p958237987(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627204487(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547328787(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p603176486(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p95029088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547469388(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p95084689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82350591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630815088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82251690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p237082692(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251337389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251226287(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p237016791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251336689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230537588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251428590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p237017391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48131791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630388990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48131891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630652887(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82152788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251388889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630192689(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251494491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251564791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251476190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251506491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p807012287(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251372589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251260288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p230474587(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251340389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251463690(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p251476390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p237011591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p237082792(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82299691(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82144488(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82089087(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82121587(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82144288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197180790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82218589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197252291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522435092(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522361390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522208187(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p522284388(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543416288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547325687(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543551889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p543068491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630194791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630194891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630019789(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630319390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630194991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630388590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630184291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630322190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630845888(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630322390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630091391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630258890(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630844188(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630258790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630217392(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630328990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630655987(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630043389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p893080087(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50132089(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630501586(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441046286(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p547261686(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441134788(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p441183089(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82318091(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82318191(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82150988(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392115490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p392115590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82105387(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p609110188(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82150688(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p609133389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p609164891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627246188(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627320490(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627367791(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p751522389(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627378291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197343392(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197224190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197131588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197143589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197111387(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197114187(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197131688(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630068489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p197155489(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630388190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48084991(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630886588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630349790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48084291(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48084391(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50249390(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50141889(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50249590(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p48092591(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630659387(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630335990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630155891(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630007589(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630370190(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50070388(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p630798688(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p50133089(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82282491(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82154688(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82093287(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82154888(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82071386(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p750261487(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82140988(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627350790(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627257588(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627333990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82155088(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p627253288(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

node p82265990(finite-states) {
kind-of-node = chance;
type-of-variable = finite-states;
relevance = 7.0;
num-states = 3;
states = (0 1 2);
}

// links of the associated graph:

link p82265990 p48124091;

link p630400490 p48124091;

link p627253288 p392115290;

link p627270088 p392115290;

link p627253288 p392150190;

link p627276488 p392150190;

link p83456290 p48109691;

link p630384190 p48109691;

link p630067789 p630384190;

link p630071089 p630384190;

link p83456290 p48109791;

link p630384190 p48109791;

link p82140988 p83456290;

link p83306289 p83456290;

link p277155690 p277195691;

link p277162190 p277195691;

link p197132888 p277155690;

link p277114088 p277155690;

link p277155690 p277195791;

link p277162190 p277195791;

link p82140988 p277162190;

link p277111088 p277162190;

link p630396290 p216124491;

link p216077190 p216124491;

link p82140988 p216077190;

link p230416387 p216077190;

link p630396290 p216124591;

link p216077190 p216124591;

link p630396290 p630182291;

link p630299990 p630182291;

link p82140988 p630299990;

link p630031389 p630299990;

link p50133089 p392157391;

link p751512889 p392157391;

link p630388390 p48147992;

link p630439091 p48147992;

link p630388390 p48148092;

link p630439091 p48148092;

link p630388390 p83567891;

link p83470790 p83567891;

link p630798688 p83470790;

link p83314589 p83470790;

link p630388190 p48084891;

link p630349790 p48084891;

link p630798688 p630323790;

link p630014189 p630323790;

link p630388190 p630155091;

link p630323790 p630155091;

link p547097990 p543072191;

link p543654190 p543072191;

link p82140988 p543654190;

link p543472088 p543654190;

link p547097990 p543072291;

link p543654190 p543072291;

link p197140688 p547097990;

link p547629489 p547097990;

link p197114187 p197140688;

link p197119188 p197140688;

link p753023491 p609183992;

link p609164891 p609183992;

link p547054590 p543036891;

link p543657690 p543036891;

link p547620489 p547054590;

link p547633289 p547054590;

link p547054590 p543036991;

link p543657690 p543036991;

link p197125588 p543517389;

link p543378087 p543517389;

link p82140988 p543657690;

link p543517389 p543657690;

link p441183089 p543084792;

link p543657690 p543084792;

link p630430091 p48127091;

link p50261091 p48127091;

link p82140988 p50261091;

link p50070388 p50261091;

link p50241090 p48111891;

link p630398790 p48111891;

link p50241090 p48172392;

link p630390690 p48172392;

link p50241090 p48172492;

link p630390690 p48172492;

link p630798688 p50241090;

link p50132089 p50241090;

link p893080087 p82236090;

link p82154988 p82236090;

link p893080087 p392120790;

link p750365488 p392120790;

link p630798688 p630328490;

link p630043389 p630328490;

link p630388190 p630152091;

link p630328890 p630152091;

link p630798688 p630328890;

link p630043389 p630328890;

link p630798688 p630282090;

link p630772188 p630282090;

link p630388590 p48112891;

link p630282090 p48112891;

link p630672087 p50168489;

link p50052788 p50168489;

link p630388590 p48128191;

link p50168489 p48128191;

link p630388590 p48139191;

link p630426691 p48139191;

link p630388590 p48139291;

link p630426691 p48139291;

link p630388590 p83572791;

link p83474091 p83572791;

link p83324189 p83474091;

link p83371889 p83474091;

link p630388590 p83572891;

link p83474091 p83572891;

link p630388590 p543068091;

link p543551889 p543068091;

link p630388590 p543068391;

link p543551889 p543068391;

link p197252291 p392203792;

link p522398991 p392203792;

link p50133089 p522398991;

link p522334189 p522398991;

link p543419788 p522334189;

link p522279888 p522334189;

link p547325687 p522279888;

link p522204687 p522279888;

link p82140988 p522369890;

link p522279888 p522369890;

link p197252291 p522449292;

link p522369890 p522449292;

link p82218589 p197240391;

link p197192390 p197240391;

link p82140988 p197192390;

link p197143789 p197192390;

link p82218589 p197240491;

link p197192390 p197240491;

link p82218589 p197258291;

link p197201290 p197258291;

link p82218589 p197258391;

link p197201290 p197258391;

link p82218589 p197258591;

link p197201290 p197258591;

link p82218589 p197303091;

link p197201290 p197303091;

link p82218589 p197303191;

link p197201290 p197303191;

link p82191389 p197201290;

link p197162589 p197201290;

link p82218589 p197303291;

link p197201290 p197303291;

link p197229090 p197288691;

link p197235390 p197288691;

link p82140988 p197149689;

link p197126088 p197149689;

link p197229090 p197318792;

link p197210690 p197318792;

link p88014589 p197210690;

link p197165689 p197210690;

link p82140988 p197165689;

link p197126088 p197165689;

link p82140988 p197206590;

link p197126088 p197206590;

link p751230786 p197126088;

link p197075886 p197126088;

link p82140988 p197229090;

link p197126088 p197229090;

link p197229090 p197288791;

link p197235390 p197288791;

link p82218589 p197276591;

link p197180690 p197276591;

link p82140988 p197180690;

link p197131388 p197180690;

link p82218589 p197252391;

link p197180790 p197252391;

link p82218589 p197252591;

link p197180790 p197252591;

link p82140988 p197206490;

link p197131388 p197206490;

link p82218589 p197256791;

link p197206490 p197256791;

link p197263491 p197353592;

link p197256791 p197353592;

link p50139889 p50195390;

link p50132089 p50195390;

link p630067989 p48004891;

link p50195390 p48004891;

link p50133089 p630172991;

link p630154989 p630172991;

link p50133089 p630173091;

link p630154989 p630173091;

link p197155489 p630147391;

link p630154689 p630147391;

link p197155489 p630147591;

link p630154689 p630147591;

link p197130688 p630154689;

link p630810388 p630154689;

link p197130688 p630154989;

link p630810388 p630154989;

link p630798688 p630067989;

link p630810388 p630067989;

link p630067989 p197263491;

link p197153289 p197263491;

link p82140988 p197153289;

link p197130288 p197153289;

link p88014589 p197235390;

link p197153289 p197235390;

link p751038090 p88127791;

link p88067190 p88127791;

link p751513589 p88067190;

link p392012788 p88067190;

link p893080087 p88014589;

link p392012788 p88014589;

link p88014589 p197211690;

link p197168789 p197211690;

link p82218589 p197314191;

link p197211690 p197314191;

link p630311090 p48022391;

link p50212890 p48022391;

link p82140988 p50212890;

link p50095388 p50212890;

link p630798688 p630311090;

link p630810288 p630311090;

link p630311090 p48022291;

link p50212890 p48022291;

link p48022291 p230057992;

link p230757791 p230057992;

link p603176686 p230565789;

link p230433487 p230565789;

link p82144288 p230757791;

link p230565789 p230757791;

link p82144288 p392087690;

link p82065086 p392087690;

link p82277591 p82349391;

link p82258390 p82349391;

link p82277591 p82345291;

link p82258290 p82345291;

link p82210989 p82258290;

link p82206789 p82258290;

link p82210989 p82258390;

link p82206789 p82258390;

link p82277591 p82349491;

link p82258390 p82349491;

link p630258790 p48000191;

link p50197590 p48000191;

link p630258790 p48000291;

link p50197590 p48000291;

link p630388590 p48110491;

link p630384990 p48110491;

link p630388590 p48110591;

link p630384990 p48110591;

link p630073589 p630384990;

link p630088089 p630384990;

link p630798688 p630062489;

link p630813788 p630062489;

link p630058889 p630040991;

link p630062489 p630040991;

link p630388190 p630154291;

link p630239990 p630154291;

link p630058889 p630239990;

link p630052589 p630239990;

link p630652587 p630058889;

link p630475686 p630058889;

link p630652587 p630073589;

link p630643987 p630073589;

link p630073589 p630266190;

link p630068889 p630266190;

link p630798688 p50241490;

link p50132089 p50241490;

link p630373290 p48072391;

link p50241490 p48072391;

link p82140988 p630373290;

link p630068889 p630373290;

link p630810088 p630068889;

link p630815588 p630068889;

link p82140988 p630373890;

link p630068889 p630373890;

link p83500691 p48197592;

link p630373890 p48197592;

link p83500691 p48213092;

link p48000691 p48213092;

link p82140988 p83403790;

link p83290288 p83403790;

link p630064189 p630249690;

link p630062389 p630249690;

link p630249690 p83500691;

link p83403790 p83500691;

link p83500691 p48213192;

link p48000691 p48213192;

link p630258790 p48000691;

link p50197590 p48000691;

link p82198489 p82243490;

link p82197489 p82243490;

link p82198489 p82243390;

link p82197489 p82243390;

link p82241490 p82292291;

link p82243390 p82292291;

link p82140988 p82241490;

link p82121587 p82241490;

link p82144288 p82218889;

link p82121587 p82218889;

link p82218889 p82347891;

link p82206489 p82347891;

link p826030789 p82338291;

link p82225690 p82338291;

link p826030789 p82338391;

link p82225690 p82338391;

link p82133387 p82206489;

link p82144488 p82206489;

link p826030789 p82307191;

link p82206489 p82307191;

link p48064091 p82422492;

link p82307191 p82422492;

link p751015990 p48064091;

link p50197190 p48064091;

link p751015990 p48064191;

link p50197190 p48064191;

link p82152588 p82247790;

link p82142888 p82247790;

link p82152588 p82238890;

link p82155088 p82238890;

link p751015990 p82334391;

link p82238890 p82334391;

link p751015990 p48064391;

link p50197190 p48064391;

link p50140189 p50197190;

link p50133689 p50197190;

link p82140988 p630258690;

link p630844188 p630258690;

link p630258690 p48013791;

link p50197390 p48013791;

link p50140189 p50197390;

link p50133689 p50197390;

link p50140189 p50197590;

link p50133689 p50197590;

link p50140189 p50197290;

link p50133689 p50197290;

link p82228290 p48007991;

link p50197290 p48007991;

link p82144288 p82228290;

link p82105287 p82228290;

link p82144288 p82228490;

link p82105287 p82228490;

link p82218289 p82277591;

link p82228490 p82277591;

link p547497788 p82218289;

link p82182889 p82218289;

link p751106191 p543151292;

link p543709391 p543151292;

link p751106191 p543151392;

link p543709391 p543151392;

link p50133089 p543709391;

link p543583789 p543709391;

link p547497788 p543583789;

link p543420288 p543583789;

link p547497788 p82266890;

link p82183689 p82266890;

link p82261490 p82332291;

link p82266890 p82332291;

link p82206389 p82261490;

link p82211489 p82261490;

link p82206389 p82303591;

link p82211489 p82303591;

link p82206389 p82303691;

link p82211489 p82303691;

link p197130888 p82211489;

link p82141488 p82211489;

link p82133387 p82206389;

link p82144488 p82206389;

link p82206389 p82261890;

link p82211489 p82261890;

link p82229690 p82366892;

link p82261890 p82366892;

link p82191289 p82229690;

link p82185289 p82229690;

link p82229690 p82280791;

link p82224990 p82280791;

link p82133387 p82224990;

link p82144488 p82224990;

link p82133387 p82225190;

link p82144488 p82225190;

link p441231590 p441290591;

link p441214590 p441290591;

link p441183089 p441324091;

link p441231890 p441324091;

link p441183089 p441324191;

link p441231890 p441324191;

link p82140988 p441231890;

link p441119488 p441231890;

link p82140988 p441231590;

link p441119488 p441231590;

link p441231590 p441290691;

link p441214590 p441290691;

link p82133387 p441214590;

link p441118988 p441214590;

link p82133387 p609134289;

link p609091487 p609134289;

link p82133387 p82225690;

link p82144488 p82225690;

link p95084689 p82350491;

link p82251690 p82350491;

link p82140988 p95141490;

link p958249187 p95141490;

link p95141490 p95247691;

link p95147490 p95247691;

link p95084689 p95147490;

link p95090289 p95147490;

link p95084689 p627412591;

link p627344390 p627412591;

link p627343790 p627396491;

link p627350790 p627396491;

link p627294789 p627343790;

link p627275788 p627343790;

link p627294789 p627344390;

link p627275788 p627344390;

link p95084689 p627412991;

link p627344390 p627412991;

link p627204487 p95029088;

link p958237987 p95029088;

link p603176486 p547469388;

link p547328787 p547469388;

link p547469388 p95084689;

link p95029088 p95084689;

link p95084689 p82350591;

link p82251690 p82350591;

link p630815088 p82251690;

link p82144488 p82251690;

link p82299691 p237082692;

link p237011591 p237082692;

link p251476390 p237016791;

link p251428590 p237016791;

link p230537588 p251428590;

link p251336689 p251428590;

link p251476390 p237017391;

link p251428590 p237017391;

link p630192689 p48131791;

link p630388990 p48131791;

link p630192689 p48131891;

link p630388990 p48131891;

link p82152788 p630192689;

link p630652887 p630192689;

link p630192689 p251494491;

link p251388889 p251494491;

link p251476190 p251564791;

link p251494491 p251564791;

link p82140988 p251476190;

link p251340389 p251476190;

link p82144288 p251506491;

link p251372589 p251506491;

link p807012287 p251372589;

link p251260288 p251372589;

link p230474587 p251340389;

link p251260288 p251340389;

link p251226287 p251463690;

link p251337389 p251463690;

link p82140988 p251476390;

link p251340389 p251476390;

link p251476390 p237011591;

link p251463690 p237011591;

link p82299691 p237082792;

link p237011591 p237082792;

link p50133089 p82299691;

link p82144488 p82299691;

link p750261487 p82144488;

link p82089087 p82144488;

link p522082985 p82121587;

link p82050786 p82121587;

link p750261487 p82144288;

link p82089087 p82144288;

link p82140988 p197180790;

link p197131388 p197180790;

link p82144288 p82218589;

link p82121587 p82218589;

link p82218589 p197252291;

link p197180790 p197252291;

link p197252291 p522435092;

link p522361390 p522435092;

link p82140988 p522361390;

link p522284388 p522361390;

link p547325687 p522284388;

link p522208187 p522284388;

link p547325687 p543551889;

link p543416288 p543551889;

link p630388590 p543068491;

link p543551889 p543068491;

link p630388590 p630194791;

link p630319390 p630194791;

link p630388590 p630194891;

link p630319390 p630194891;

link p630798688 p630319390;

link p630019789 p630319390;

link p630388590 p630194991;

link p630319390 p630194991;

link p630388590 p630184291;

link p630322190 p630184291;

link p630798688 p630322190;

link p630845888 p630322190;

link p630798688 p630322390;

link p630845888 p630322390;

link p630258890 p630091391;

link p630322390 p630091391;

link p82140988 p630258890;

link p630844188 p630258890;

link p82140988 p630258790;

link p630844188 p630258790;

link p630258790 p630217392;

link p630328990 p630217392;

link p630798688 p630328990;

link p630043389 p630328990;

link p893080087 p630043389;

link p630655987 p630043389;

link p893080087 p50132089;

link p630501586 p50132089;

link p547261686 p441134788;

link p441046286 p441134788;

link p82150688 p441183089;

link p441134788 p441183089;

link p50133089 p82318091;

link p82150988 p82318091;

link p50133089 p82318191;

link p82150988 p82318191;

link p750261487 p82150988;

link p82105387 p82150988;

link p82140988 p392115490;

link p82105387 p392115490;

link p82140988 p392115590;

link p82105387 p392115590;

link p750261487 p82150688;

link p82105387 p82150688;

link p82150688 p609133389;

link p609110188 p609133389;

link p197143589 p609164891;

link p609133389 p609164891;

link p627253288 p627320490;

link p627246188 p627320490;

link p751522389 p627367791;

link p627320490 p627367791;

link p751522389 p627378291;

link p627333990 p627378291;

link p627378291 p197343392;

link p197224190 p197343392;

link p82140988 p197224190;

link p197131588 p197224190;

link p197114187 p197131588;

link p197111387 p197131588;

link p197114187 p197143589;

link p197111387 p197143589;

link p197114187 p197131688;

link p197111387 p197131688;

link p630798688 p630068489;

link p630810388 p630068489;

link p82140988 p197155489;

link p197131688 p197155489;

link p197155489 p630388190;

link p630068489 p630388190;

link p630388190 p48084991;

link p630349790 p48084991;

link p630798688 p630349790;

link p630886588 p630349790;

link p630370190 p48084291;

link p50249390 p48084291;

link p630370190 p48084391;

link p50249390 p48084391;

link p82140988 p50249390;

link p50141889 p50249390;

link p82140988 p50249590;

link p50141889 p50249590;

link p630370190 p48092591;

link p50249590 p48092591;

link p82140988 p630335990;

link p630659387 p630335990;

link p630370190 p630155891;

link p630335990 p630155891;

link p630798688 p630370190;

link p630007589 p630370190;

link p547261686 p50070388;

link p630501586 p50070388;

link p630798688 p50133089;

link p50070388 p50133089;

link p50133089 p82282491;

link p82154888 p82282491;

link p750261487 p82154688;

link p82093287 p82154688;

link p750261487 p82154888;

link p82093287 p82154888;

link p803043885 p82071386;

link p82019685 p82071386;

link p750261487 p82140988;

link p82071386 p82140988;

link p82140988 p627350790;

link p627257588 p627350790;

link p627253288 p627333990;

link p627257588 p627333990;

link p627253288 p82265990;

link p82155088 p82265990;

//Network Relationships: 

relation p630400490 { 
values= table (0.25 0.5 0.25 );
}

relation p48124091 p82265990 p630400490 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627270088 { 
values= table (0.25 0.5 0.25 );
}

relation p392115290 p627253288 p627270088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627276488 { 
values= table (0.25 0.5 0.25 );
}

relation p392150190 p627253288 p627276488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48109691 p83456290 p630384190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630071089 { 
values= table (0.25 0.5 0.25 );
}

relation p630067789 { 
values= table (0.25 0.5 0.25 );
}

relation p630384190 p630067789 p630071089 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48109791 p83456290 p630384190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83306289 { 
values= table (0.25 0.5 0.25 );
}

relation p83456290 p82140988 p83306289 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p277195691 p277155690 p277162190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p277114088 { 
values= table (0.25 0.5 0.25 );
}

relation p197132888 { 
values= table (0.25 0.5 0.25 );
}

relation p277155690 p197132888 p277114088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p277195791 p277155690 p277162190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p277111088 { 
values= table (0.25 0.5 0.25 );
}

relation p277162190 p82140988 p277111088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p216124491 p630396290 p216077190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p230416387 { 
values= table (0.25 0.5 0.25 );
}

relation p216077190 p82140988 p230416387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p216124591 p630396290 p216077190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630396290 { 
values= table (0.25 0.5 0.25 );
}

relation p630182291 p630396290 p630299990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630031389 { 
values= table (0.25 0.5 0.25 );
}

relation p630299990 p82140988 p630031389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82019685 { 
values= table (0.25 0.5 0.25 );
}

relation p803043885 { 
values= table (0.25 0.5 0.25 );
}

relation p751512889 { 
values= table (0.25 0.5 0.25 );
}

relation p392157391 p50133089 p751512889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48147992 p630388390 p630439091 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630439091 { 
values= table (0.25 0.5 0.25 );
}

relation p48148092 p630388390 p630439091 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630388390 { 
values= table (0.25 0.5 0.25 );
}

relation p83567891 p630388390 p83470790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83314589 { 
values= table (0.25 0.5 0.25 );
}

relation p83470790 p630798688 p83314589 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48084891 p630388190 p630349790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630014189 { 
values= table (0.25 0.5 0.25 );
}

relation p630323790 p630798688 p630014189 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630155091 p630388190 p630323790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543072191 p547097990 p543654190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543472088 { 
values= table (0.25 0.5 0.25 );
}

relation p543654190 p82140988 p543472088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543072291 p547097990 p543654190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p547629489 { 
values= table (0.25 0.5 0.25 );
}

relation p547097990 p197140688 p547629489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197119188 { 
values= table (0.25 0.5 0.25 );
}

relation p197140688 p197114187 p197119188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p753023491 { 
values= table (0.25 0.5 0.25 );
}

relation p609183992 p753023491 p609164891 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543036891 p547054590 p543657690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p547633289 { 
values= table (0.25 0.5 0.25 );
}

relation p547620489 { 
values= table (0.25 0.5 0.25 );
}

relation p547054590 p547620489 p547633289 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543036991 p547054590 p543657690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543378087 { 
values= table (0.25 0.5 0.25 );
}

relation p197125588 { 
values= table (0.25 0.5 0.25 );
}

relation p543517389 p197125588 p543378087 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543657690 p82140988 p543517389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543084792 p441183089 p543657690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630430091 { 
values= table (0.25 0.5 0.25 );
}

relation p48127091 p630430091 p50261091 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50261091 p82140988 p50070388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630398790 { 
values= table (0.25 0.5 0.25 );
}

relation p48111891 p50241090 p630398790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48172392 p50241090 p630390690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630390690 { 
values= table (0.25 0.5 0.25 );
}

relation p48172492 p50241090 p630390690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50241090 p630798688 p50132089 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82154988 { 
values= table (0.25 0.5 0.25 );
}

relation p82236090 p893080087 p82154988 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p750365488 { 
values= table (0.25 0.5 0.25 );
}

relation p392120790 p893080087 p750365488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630328490 p630798688 p630043389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630152091 p630388190 p630328890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630328890 p630798688 p630043389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630772188 { 
values= table (0.25 0.5 0.25 );
}

relation p630282090 p630798688 p630772188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48112891 p630388590 p630282090 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50052788 { 
values= table (0.25 0.5 0.25 );
}

relation p630672087 { 
values= table (0.25 0.5 0.25 );
}

relation p50168489 p630672087 p50052788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48128191 p630388590 p50168489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48139191 p630388590 p630426691 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630426691 { 
values= table (0.25 0.5 0.25 );
}

relation p48139291 p630388590 p630426691 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83572791 p630388590 p83474091 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83371889 { 
values= table (0.25 0.5 0.25 );
}

relation p83324189 { 
values= table (0.25 0.5 0.25 );
}

relation p83474091 p83324189 p83371889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83572891 p630388590 p83474091 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543068091 p630388590 p543551889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543068391 p630388590 p543551889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p392203792 p197252291 p522398991 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522398991 p50133089 p522334189 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543419788 { 
values= table (0.25 0.5 0.25 );
}

relation p522334189 p543419788 p522279888 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522204687 { 
values= table (0.25 0.5 0.25 );
}

relation p522279888 p547325687 p522204687 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522369890 p82140988 p522279888 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522449292 p197252291 p522369890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197240391 p82218589 p197192390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197143789 { 
values= table (0.25 0.5 0.25 );
}

relation p197192390 p82140988 p197143789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197240491 p82218589 p197192390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197258291 p82218589 p197201290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197258391 p82218589 p197201290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197258591 p82218589 p197201290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197303091 p82218589 p197201290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197303191 p82218589 p197201290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197162589 { 
values= table (0.25 0.5 0.25 );
}

relation p82191389 { 
values= table (0.25 0.5 0.25 );
}

relation p197201290 p82191389 p197162589 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197303291 p82218589 p197201290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197288691 p197229090 p197235390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197149689 p82140988 p197126088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197318792 p197229090 p197210690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197210690 p88014589 p197165689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197165689 p82140988 p197126088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197206590 p82140988 p197126088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197075886 { 
values= table (0.25 0.5 0.25 );
}

relation p751230786 { 
values= table (0.25 0.5 0.25 );
}

relation p197126088 p751230786 p197075886 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197229090 p82140988 p197126088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197288791 p197229090 p197235390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197276591 p82218589 p197180690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197180690 p82140988 p197131388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197252391 p82218589 p197180790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197252591 p82218589 p197180790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197131388 { 
values= table (0.25 0.5 0.25 );
}

relation p197206490 p82140988 p197131388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197256791 p82218589 p197206490 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197353592 p197263491 p197256791 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50139889 { 
values= table (0.25 0.5 0.25 );
}

relation p50195390 p50139889 p50132089 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48004891 p630067989 p50195390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630172991 p50133089 p630154989 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630173091 p50133089 p630154989 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630147391 p197155489 p630154689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630147591 p197155489 p630154689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630154689 p197130688 p630810388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197130688 { 
values= table (0.25 0.5 0.25 );
}

relation p630154989 p197130688 p630810388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630810388 { 
values= table (0.25 0.5 0.25 );
}

relation p630067989 p630798688 p630810388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197263491 p630067989 p197153289 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197130288 { 
values= table (0.25 0.5 0.25 );
}

relation p197153289 p82140988 p197130288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197235390 p88014589 p197153289 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p751038090 { 
values= table (0.25 0.5 0.25 );
}

relation p88127791 p751038090 p88067190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p751513589 { 
values= table (0.25 0.5 0.25 );
}

relation p88067190 p751513589 p392012788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p392012788 { 
values= table (0.25 0.5 0.25 );
}

relation p197168789 { 
values= table (0.25 0.5 0.25 );
}

relation p88014589 p893080087 p392012788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197211690 p88014589 p197168789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197314191 p82218589 p197211690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50095388 { 
values= table (0.25 0.5 0.25 );
}

relation p48022391 p630311090 p50212890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630810288 { 
values= table (0.25 0.5 0.25 );
}

relation p50212890 p82140988 p50095388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630311090 p630798688 p630810288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48022291 p630311090 p50212890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p230057992 p48022291 p230757791 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p230433487 { 
values= table (0.25 0.5 0.25 );
}

relation p603176686 { 
values= table (0.25 0.5 0.25 );
}

relation p230565789 p603176686 p230433487 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p230757791 p82144288 p230565789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82065086 { 
values= table (0.25 0.5 0.25 );
}

relation p392087690 p82144288 p82065086 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82349391 p82277591 p82258390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82345291 p82277591 p82258290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82258290 p82210989 p82206789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82206789 { 
values= table (0.25 0.5 0.25 );
}

relation p82210989 { 
values= table (0.25 0.5 0.25 );
}

relation p82258390 p82210989 p82206789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82349491 p82277591 p82258390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48000191 p630258790 p50197590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48000291 p630258790 p50197590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48110491 p630388590 p630384990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48110591 p630388590 p630384990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630088089 { 
values= table (0.25 0.5 0.25 );
}

relation p630384990 p630073589 p630088089 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630813788 { 
values= table (0.25 0.5 0.25 );
}

relation p630062489 p630798688 p630813788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630040991 p630058889 p630062489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630154291 p630388190 p630239990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630052589 { 
values= table (0.25 0.5 0.25 );
}

relation p630239990 p630058889 p630052589 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630475686 { 
values= table (0.25 0.5 0.25 );
}

relation p630058889 p630652587 p630475686 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630643987 { 
values= table (0.25 0.5 0.25 );
}

relation p630652587 { 
values= table (0.25 0.5 0.25 );
}

relation p630073589 p630652587 p630643987 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630266190 p630073589 p630068889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50241490 p630798688 p50132089 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48072391 p630373290 p50241490 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630373290 p82140988 p630068889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630815588 { 
values= table (0.25 0.5 0.25 );
}

relation p630810088 { 
values= table (0.25 0.5 0.25 );
}

relation p630068889 p630810088 p630815588 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630373890 p82140988 p630068889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48197592 p83500691 p630373890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48213092 p83500691 p48000691 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83290288 { 
values= table (0.25 0.5 0.25 );
}

relation p630062389 { 
values= table (0.25 0.5 0.25 );
}

relation p630064189 { 
values= table (0.25 0.5 0.25 );
}

relation p83403790 p82140988 p83290288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630249690 p630064189 p630062389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p83500691 p630249690 p83403790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48213192 p83500691 p48000691 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48000691 p630258790 p50197590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82243490 p82198489 p82197489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82197489 { 
values= table (0.25 0.5 0.25 );
}

relation p82198489 { 
values= table (0.25 0.5 0.25 );
}

relation p82243390 p82198489 p82197489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82292291 p82241490 p82243390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82241490 p82140988 p82121587 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82050786 { 
values= table (0.25 0.5 0.25 );
}

relation p522082985 { 
values= table (0.25 0.5 0.25 );
}

relation p82218889 p82144288 p82121587 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82347891 p82218889 p82206489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82338291 p826030789 p82225690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82338391 p826030789 p82225690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82206489 p82133387 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p826030789 { 
values= table (0.25 0.5 0.25 );
}

relation p82307191 p826030789 p82206489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82422492 p48064091 p82307191 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48064091 p751015990 p50197190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48064191 p751015990 p50197190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82142888 { 
values= table (0.25 0.5 0.25 );
}

relation p82247790 p82152588 p82142888 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82152588 { 
values= table (0.25 0.5 0.25 );
}

relation p82238890 p82152588 p82155088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82334391 p751015990 p82238890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p751015990 { 
values= table (0.25 0.5 0.25 );
}

relation p48064391 p751015990 p50197190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50197190 p50140189 p50133689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630258690 p82140988 p630844188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48013791 p630258690 p50197390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50197390 p50140189 p50133689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50197590 p50140189 p50133689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50133689 { 
values= table (0.25 0.5 0.25 );
}

relation p50140189 { 
values= table (0.25 0.5 0.25 );
}

relation p50197290 p50140189 p50133689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48007991 p82228290 p50197290 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82228290 p82144288 p82105287 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82105287 { 
values= table (0.25 0.5 0.25 );
}

relation p82228490 p82144288 p82105287 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82277591 p82218289 p82228490 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82182889 { 
values= table (0.25 0.5 0.25 );
}

relation p82218289 p547497788 p82182889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543151292 p751106191 p543709391 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p751106191 { 
values= table (0.25 0.5 0.25 );
}

relation p543151392 p751106191 p543709391 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543709391 p50133089 p543583789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543420288 { 
values= table (0.25 0.5 0.25 );
}

relation p543583789 p547497788 p543420288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82183689 { 
values= table (0.25 0.5 0.25 );
}

relation p547497788 { 
values= table (0.25 0.5 0.25 );
}

relation p82266890 p547497788 p82183689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82332291 p82261490 p82266890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82261490 p82206389 p82211489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82303591 p82206389 p82211489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82141488 { 
values= table (0.25 0.5 0.25 );
}

relation p197130888 { 
values= table (0.25 0.5 0.25 );
}

relation p82303691 p82206389 p82211489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82211489 p197130888 p82141488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82206389 p82133387 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82261890 p82206389 p82211489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82366892 p82229690 p82261890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82185289 { 
values= table (0.25 0.5 0.25 );
}

relation p82191289 { 
values= table (0.25 0.5 0.25 );
}

relation p82229690 p82191289 p82185289 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82280791 p82229690 p82224990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82224990 p82133387 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82225190 p82133387 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441290591 p441231590 p441214590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441324091 p441183089 p441231890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441324191 p441183089 p441231890 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441231890 p82140988 p441119488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441119488 { 
values= table (0.25 0.5 0.25 );
}

relation p441231590 p82140988 p441119488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441290691 p441231590 p441214590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441118988 { 
values= table (0.25 0.5 0.25 );
}

relation p441214590 p82133387 p441118988 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p609091487 { 
values= table (0.25 0.5 0.25 );
}

relation p609134289 p82133387 p609091487 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82133387 { 
values= table (0.25 0.5 0.25 );
}

relation p82225690 p82133387 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82350491 p95084689 p82251690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p958249187 { 
values= table (0.25 0.5 0.25 );
}

relation p95141490 p82140988 p958249187 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p95247691 p95141490 p95147490 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p95090289 { 
values= table (0.25 0.5 0.25 );
}

relation p95147490 p95084689 p95090289 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627412591 p95084689 p627344390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627396491 p627343790 p627350790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627343790 p627294789 p627275788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627275788 { 
values= table (0.25 0.5 0.25 );
}

relation p627294789 { 
values= table (0.25 0.5 0.25 );
}

relation p627344390 p627294789 p627275788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627412991 p95084689 p627344390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p958237987 { 
values= table (0.25 0.5 0.25 );
}

relation p627204487 { 
values= table (0.25 0.5 0.25 );
}

relation p547328787 { 
values= table (0.25 0.5 0.25 );
}

relation p603176486 { 
values= table (0.25 0.5 0.25 );
}

relation p95029088 p627204487 p958237987 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p547469388 p603176486 p547328787 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p95084689 p547469388 p95029088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82350591 p95084689 p82251690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630815088 { 
values= table (0.25 0.5 0.25 );
}

relation p82251690 p630815088 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p237082692 p82299691 p237011591 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251337389 { 
values= table (0.25 0.5 0.25 );
}

relation p251226287 { 
values= table (0.25 0.5 0.25 );
}

relation p237016791 p251476390 p251428590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251336689 { 
values= table (0.25 0.5 0.25 );
}

relation p230537588 { 
values= table (0.25 0.5 0.25 );
}

relation p251428590 p230537588 p251336689 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p237017391 p251476390 p251428590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48131791 p630192689 p630388990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630388990 { 
values= table (0.25 0.5 0.25 );
}

relation p48131891 p630192689 p630388990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630652887 { 
values= table (0.25 0.5 0.25 );
}

relation p82152788 { 
values= table (0.25 0.5 0.25 );
}

relation p251388889 { 
values= table (0.25 0.5 0.25 );
}

relation p630192689 p82152788 p630652887 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251494491 p630192689 p251388889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251564791 p251476190 p251494491 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251476190 p82140988 p251340389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251506491 p82144288 p251372589 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p807012287 { 
values= table (0.25 0.5 0.25 );
}

relation p251372589 p807012287 p251260288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251260288 { 
values= table (0.25 0.5 0.25 );
}

relation p230474587 { 
values= table (0.25 0.5 0.25 );
}

relation p251340389 p230474587 p251260288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251463690 p251226287 p251337389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p251476390 p82140988 p251340389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p237011591 p251476390 p251463690 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p237082792 p82299691 p237011591 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82299691 p50133089 p82144488 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82144488 p750261487 p82089087 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82089087 { 
values= table (0.25 0.5 0.25 );
}

relation p82121587 p522082985 p82050786 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82144288 p750261487 p82089087 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197180790 p82140988 p197131388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82218589 p82144288 p82121587 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197252291 p82218589 p197180790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522435092 p197252291 p522361390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522361390 p82140988 p522284388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p522208187 { 
values= table (0.25 0.5 0.25 );
}

relation p522284388 p547325687 p522208187 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543416288 { 
values= table (0.25 0.5 0.25 );
}

relation p547325687 { 
values= table (0.25 0.5 0.25 );
}

relation p543551889 p547325687 p543416288 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p543068491 p630388590 p543551889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630194791 p630388590 p630319390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630194891 p630388590 p630319390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630019789 { 
values= table (0.25 0.5 0.25 );
}

relation p630319390 p630798688 p630019789 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630194991 p630388590 p630319390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630388590 { 
values= table (0.25 0.5 0.25 );
}

relation p630184291 p630388590 p630322190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630322190 p630798688 p630845888 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630845888 { 
values= table (0.25 0.5 0.25 );
}

relation p630322390 p630798688 p630845888 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630091391 p630258890 p630322390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630258890 p82140988 p630844188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630844188 { 
values= table (0.25 0.5 0.25 );
}

relation p630258790 p82140988 p630844188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630217392 p630258790 p630328990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630328990 p630798688 p630043389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630655987 { 
values= table (0.25 0.5 0.25 );
}

relation p630043389 p893080087 p630655987 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p893080087 { 
values= table (0.25 0.5 0.25 );
}

relation p50132089 p893080087 p630501586 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630501586 { 
values= table (0.25 0.5 0.25 );
}

relation p441046286 { 
values= table (0.25 0.5 0.25 );
}

relation p547261686 { 
values= table (0.25 0.5 0.25 );
}

relation p441134788 p547261686 p441046286 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p441183089 p82150688 p441134788 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82318091 p50133089 p82150988 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82318191 p50133089 p82150988 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82150988 p750261487 p82105387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p392115490 p82140988 p82105387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p392115590 p82140988 p82105387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82105387 { 
values= table (0.25 0.5 0.25 );
}

relation p609110188 { 
values= table (0.25 0.5 0.25 );
}

relation p82150688 p750261487 p82105387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p609133389 p82150688 p609110188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p609164891 p197143589 p609133389 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627246188 { 
values= table (0.25 0.5 0.25 );
}

relation p627320490 p627253288 p627246188 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627367791 p751522389 p627320490 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p751522389 { 
values= table (0.25 0.5 0.25 );
}

relation p627378291 p751522389 p627333990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197343392 p627378291 p197224190 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197224190 p82140988 p197131588 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197131588 p197114187 p197111387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197143589 p197114187 p197111387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197111387 { 
values= table (0.25 0.5 0.25 );
}

relation p197114187 { 
values= table (0.25 0.5 0.25 );
}

relation p197131688 p197114187 p197111387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630068489 p630798688 p630810388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p197155489 p82140988 p197131688 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630388190 p197155489 p630068489 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48084991 p630388190 p630349790 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630886588 { 
values= table (0.25 0.5 0.25 );
}

relation p630349790 p630798688 p630886588 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48084291 p630370190 p50249390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48084391 p630370190 p50249390 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50249390 p82140988 p50141889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50141889 { 
values= table (0.25 0.5 0.25 );
}

relation p50249590 p82140988 p50141889 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p48092591 p630370190 p50249590 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630659387 { 
values= table (0.25 0.5 0.25 );
}

relation p630335990 p82140988 p630659387 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630155891 p630370190 p630335990 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630007589 { 
values= table (0.25 0.5 0.25 );
}

relation p630370190 p630798688 p630007589 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p50070388 p547261686 p630501586 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p630798688 { 
values= table (0.25 0.5 0.25 );
}

relation p50133089 p630798688 p50070388 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82282491 p50133089 p82154888 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82154688 p750261487 p82093287 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82093287 { 
values= table (0.25 0.5 0.25 );
}

relation p82154888 p750261487 p82093287 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82071386 p803043885 p82019685 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p750261487 { 
values= table (0.25 0.5 0.25 );
}

relation p82140988 p750261487 p82071386 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627350790 p82140988 p627257588 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p627257588 { 
values= table (0.25 0.5 0.25 );
}

relation p627333990 p627253288 p627257588 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

relation p82155088 { 
values= table (0.25 0.5 0.25 );
}

relation p627253288 { 
values= table (0.25 0.5 0.25 );
}

relation p82265990 p627253288 p82155088 { 
values= table (1.0 0.5 0.0 0.5 0.25 0.0 0.0 0.0 0.0 0.0 0.5 1.0 0.5 0.5 0.5 1.0 0.5 0.0 0.0 0.0 0.0 0.0 0.25 0.5 0.0 0.5 1.0 );
}

}
