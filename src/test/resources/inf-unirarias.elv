// Influence Diagram
//   Elvira format 

idiagram  Untitled1 { 

// Network Properties

visualprecision = "0.00";
version = 1.0;
default node states = (present , absent);

// Network Variables 

node Sonda(finite-states) {
title = "Sonda uretral";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =234;
pos_y =55;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (si no);
}

node Infecci�n(finite-states) {
title = "Infecci�n inicio";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =229;
pos_y =160;
relevance = 7.0;
purpose = "";
num-states = 4;
states = (gram_positivo gram_negativo levaduras ninguno);
}

node Infecci�n_2(finite-states) {
title = "Infecci�n 48 h";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =667;
pos_y =163;
relevance = 7.0;
purpose = "";
num-states = 4;
states = (gram_positivo gram_negativo levaduras ninguno);
}

node D(finite-states) {
title = "Fiebre";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =98;
pos_y =279;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node E(finite-states) {
title = "Malestar general";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =157;
pos_y =354;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node F(finite-states) {
title = "Molestias orinar";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =246;
pos_y =298;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node G(finite-states) {
title = "S. confusional";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =313;
pos_y =353;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node D_1(finite-states) {
title = "Fiebre 48 h";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =586;
pos_y =286;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node E_1(finite-states) {
title = "Mal. general 48 h";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =594;
pos_y =399;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node F_1(finite-states) {
title = "Mol. orinar 48 h";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =693;
pos_y =301;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node G_1(finite-states) {
title = "S. conf. 48 h";
kind-of-node = chance;
type-of-variable = finite-states;
pos_x =693;
pos_y =359;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

node D4(finite-states) {
kind-of-node = decision;
type-of-variable = finite-states;
pos_x =442;
pos_y =265;
relevance = 7.0;
purpose = "";
num-states = 2;
states = (present absent);
}

// links of the associated graph:

link Sonda Infecci�n;

link Infecci�n D;

link Infecci�n E;

link Infecci�n F;

link Infecci�n G;

link Infecci�n_2 D_1;

link Infecci�n_2 E_1;

link Infecci�n_2 F_1;

link Infecci�n_2 G_1;

link Infecci�n Infecci�n_2;

link D D4;

link F D4;

link E D4;

link G D4;

link Sonda D4;

link D4 Infecci�n_2;

//Network Relationships: 

relation Sonda { 
comment = "new";
values= table (0.5 0.5 );
}

relation Infecci�n Sonda { 
comment = "new";
values= table (0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 );
}

relation D Infecci�n { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation E Infecci�n { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation F Infecci�n { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation G Infecci�n { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation D_1 Infecci�n Infecci�n_2 { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation E_1 Infecci�n Infecci�n_2 { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation F_1 Infecci�n Infecci�n_2 { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation G_1 Infecci�n Infecci�n_2 { 
comment = "new";
values= table (0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 );
}

relation Infecci�n_2 Infecci�n D4 { 
comment = "new";
values= table (0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.25 );
}

}
