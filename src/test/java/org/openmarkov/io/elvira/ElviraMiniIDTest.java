/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.io.elvira;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.VariableType;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;

/** @author marias
 * @vesion 1.0 */
public class ElviraMiniIDTest {

	// Attributes
	private static String testFile = "trivial3jensen.elv";
	
	private ProbNet probNet;
	
	private Node nodeA;

	private Node nodeD1;

	private Node nodeD2;

	private Variable variableA;
	
	private Variable variableD1;
	
	private Variable variableD2;
	
	private Variable variableU;
	
	private List<Potential> utilityPotentials;
	
	@Before
	/** Create a ElviraScanner and opens a file for tests */
	public void setUp() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(testFile);
		probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
		nodeA = probNet.getNode("A");
		nodeD1 = probNet.getNode("D1");
		nodeD2 = probNet.getNode("D2");
		variableA = nodeA.getVariable();
		variableD1 = nodeD1.getVariable();
		variableD2 = nodeD2.getVariable();
		variableU = probNet.getVariable("U");
		utilityPotentials = probNet.getUtilityPotentials(variableD2);
	}
	
	@Test
	public void testDecisionAndUtilityVariables() {
		assertNotNull(variableD1);
		assertNotNull(variableD2);
		assertNotNull(variableU);
		
		assertEquals(3, variableD1.getNumStates());
		assertEquals(2, variableD2.getNumStates());

		assertEquals(VariableType.FINITE_STATES, variableD1.getVariableType());
		assertEquals(VariableType.FINITE_STATES, variableD2.getVariableType());
		assertEquals(VariableType.NUMERIC, variableU.getVariableType());
	}
	
	@Test
	public void testUtilityPotential() {
		assertEquals(1, utilityPotentials.size());
		TablePotential utilityPotential = 
			(TablePotential)utilityPotentials.get(0);
		List<Variable> utilityVariables = utilityPotential.getVariables();
		assertEquals(2, utilityVariables.size());
		assertEquals(variableA, utilityVariables.get(0));
		assertEquals(variableD2, utilityVariables.get(1));
		assertTrue(utilityPotential.isUtility());
		assertEquals(variableU, utilityPotential.getUtilityVariable());
	}
	
}