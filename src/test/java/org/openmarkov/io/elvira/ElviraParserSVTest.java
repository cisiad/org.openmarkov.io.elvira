/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.io.elvira;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.State;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;

/** @author marias
 * @vesion 1.0 */
public class ElviraParserSVTest {
	
	// Constants
	private double maxError = 1E-5;
	
	// Attributes
	private static String testFile = "ejemplo_Tatman_y_Shachter.elv";
	
	private ProbNet probNet;
	
	@Before
	/** Create a ElviraScanner and opens a file for tests */
	public void setUp() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(testFile);
		probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
	}
	
	// Unit tests
	@Test
	public void getGeneralInfo() throws Exception {
		//String kindOfGraph = (String)probNet.properties.get("KindOfGraph");
		//assertNotNull(kindOfGraph);
		//assertTrue(kindOfGraph.contentEquals("directed"));
		Double version = Double.parseDouble(
				probNet.additionalProperties.get("Version"));
		assertEquals(1.0, version.doubleValue(), maxError);
		State[] defaultNodeStates = 
			(State[])probNet.getDefaultStates();
		assertNotNull(defaultNodeStates);
		assertEquals(2, defaultNodeStates.length);
		assertTrue(defaultNodeStates[0].getName().contentEquals("absent"));
		assertTrue(defaultNodeStates[1].getName().contentEquals("present"));
	}
	
	@Test
	public void getNodes() throws Exception {
		assertEquals(8, probNet.getNumNodes()); // Test number of nodes
		// Test some attributes of one random node
		String nameOfVariable = "A"; 
		Node node = probNet.getNode(nameOfVariable);
		assertNotNull(node);
		Variable variable = node.getVariable();
		assertEquals(2, variable.getNumStates());
		assertEquals(0, variable.getStateIndex("absent"));
		Integer posX = (int)node.getCoordinateX();
		assertNotNull(posX);
		assertEquals(229, posX.intValue());
		// Test variables
		List<Variable> variables = probNet.getVariables();
		assertEquals(8, variables.size());
	}
	
	@Test
	public void getLinks() throws Exception {
		// Test children of one random node
		String nameOfVariable = "A"; 
		Node node = probNet.getNode(nameOfVariable);
		List<Node> children = node.getChildren();
		assertEquals(3, children.size());
	}

	@Test
	public void getPotentials() throws Exception {
	    List<Potential> potentials = probNet.getPotentials();
		assertEquals(7, potentials.size());
		// Test a canonical potential
		// Gets variable Quality_of_life_U1
		Variable U1Variable = probNet.getVariable("U3");
		assertNotNull(U1Variable);
		// Gets potentials corresponding to variable Quality_of_life_U1
		List<Potential> U1Potentials = probNet.getPotentials(U1Variable);
		assertEquals(2, U1Potentials.size());
		TablePotential U1Potential = (TablePotential) U1Potentials.get(0);
		// Test sub-potentials
		/*ArrayList<TablePotential> U1SubPotentials = 
			U1Potential.getSubPotentials();*/
/*		for (TablePotential potential : U1SubPotentials) {
			ArrayList<Variable> variables = potential.getVariables();
				// Test conversion to OpenMarkov potential
				// Test ordination of variables
				assertEquals(U1Variable, variables.get(0));*/
				// Test potential table
		//System.out.println(U1Potential.table[1]);
		assertEquals(0.0, U1Potential.values[0], maxError);
		assertEquals(1.0, U1Potential.values[1], maxError);
		assertEquals(3.0, U1Potential.values[2], maxError);
		assertEquals(2.0, U1Potential.values[3], maxError);
//			}
		}
}
