package org.openmarkov.io.elvira;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.model.network.ProbNet;

public class ElviraIDsTest {

	// Attributes
	private static String testFile = "IDU1-mediastinet.elv";
	
	private ProbNet probNet;
	
	@Before
	/** Create a ElviraScanner and opens a file for tests */
	public void setUp() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(testFile);
		System.out.println(testFile);
		System.out.println("-------------------------------------");
		System.out.println();
		probNet = new ElviraParser ().loadProbNet(url.getFile()).getProbNet();
	}
	
	@Test
	public void test() {
		assertNotNull(probNet);
	}

}
