/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.io.elvira;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;

/** @author marias
 * @vesion 1.0 */
public class ElviraMiniredesTest {
	
	// Constants
	private double maxError = 1E-5;

	// Attributes
	private static String testFile = "peque.elv";
	
	private ProbNet probNet;
	
	private Node nodeA;

	private Node nodeB;

	private Node nodeC;
	
	private Variable variableA;

	private Variable variableB;

	private Variable variableC;
	
	private List<Potential> potentialsWithA;

	private List<Potential> potentialsWithB;

	private List<Potential> potentialsWithC;

	@Before
	/** Create a ElviraScanner and opens a file for tests */
	public void setUp() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(testFile);
		probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
		nodeA = probNet.getNode("A");
		nodeB = probNet.getNode("B");
		nodeC = probNet.getNode("C");
		variableA = nodeA.getVariable();
		variableB = nodeB.getVariable();
		variableC = nodeC.getVariable();
		potentialsWithA = probNet.getPotentials(variableA);
		potentialsWithB = probNet.getPotentials(variableB);
		potentialsWithC = probNet.getPotentials(variableC);
	}
	
	@Test
	public void testNodes() {
		assertNotNull(nodeA);
		assertNotNull(nodeB);
		assertNotNull(nodeC);
	}
	
	@Test
	public void testLinks() {
		List<Node> childrenA = nodeA.getChildren();
		assertEquals(2, childrenA.size());
		assertTrue(childrenA.contains(nodeB));
		assertTrue(childrenA.contains(nodeC));
		List<Node> childrenB = nodeB.getChildren();
		assertEquals(1, childrenB.size());
		assertTrue(childrenB.contains(nodeC));
	}
	
	@Test
	public void testRelations() {
		assertEquals(3, potentialsWithA.size());
		assertEquals(2, potentialsWithB.size());
		assertEquals(1, potentialsWithC.size());
		potentialsWithA.removeAll(potentialsWithB); // gets only p(A)
		potentialsWithB.removeAll(potentialsWithC); // gets only p(B|A))
		// Test potentials
		// Potential P(A)
		TablePotential probA = (TablePotential)potentialsWithA.get(0);
		List<Variable> variablesA = probA.getVariables();
		assertEquals(1, variablesA.size());
		assertEquals(variableA, variablesA.get(0));
		// A probabilities
		assertEquals(2, probA.values.length);
		assertEquals(0.2, probA.values[0], maxError);
		assertEquals(0.8, probA.values[1], maxError);
		// Potential P(B|A)
		TablePotential probBA = (TablePotential)potentialsWithB.get(0);
		List<Variable> variablesBA = probBA.getVariables();
		assertEquals(2, variablesBA.size());
		assertEquals(variableB, variablesBA.get(0));
		assertEquals(variableA, variablesBA.get(1));
		// A probabilities
		assertEquals(4, probBA.values.length);
		assertEquals(0.7, probBA.values[0], maxError);
		assertEquals(0.3, probBA.values[1], maxError);
		assertEquals(0.9, probBA.values[2], maxError);
		assertEquals(0.1, probBA.values[3], maxError);
		// Potential P(C|B,A)
		TablePotential probCBA = (TablePotential)potentialsWithC.get(0);
		List<Variable> variablesCBA = probCBA.getVariables();
		assertEquals(3, variablesCBA.size());
		assertEquals(variableC, variablesCBA.get(0));
		assertEquals(variableA, variablesCBA.get(1));
		assertEquals(variableB, variablesCBA.get(2));
		// A probabilities
		assertEquals(8, probCBA.values.length);
		assertEquals(0.15, probCBA.values[0], maxError);
		assertEquals(0.85, probCBA.values[1], maxError);
		assertEquals(0.84, probCBA.values[2], maxError);
		assertEquals(0.16, probCBA.values[3], maxError);
		assertEquals(0.29, probCBA.values[4], maxError);
		assertEquals(0.71, probCBA.values[5], maxError);
		assertEquals(0.98, probCBA.values[6], maxError);
		assertEquals(0.02, probCBA.values[7], maxError);
	}

}
