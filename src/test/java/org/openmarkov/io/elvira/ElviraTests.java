/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.io.elvira;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ElviraScannerTest.class,
	ElviraMiniredesTest.class,
	ElviraMiniIDTest.class,
	ElviraIDsTest.class,
	ElviraReaderCanonicModelsTest.class,
	ElviraParserTest.class,
//	ElviraParserSVTest.class, // This test is eliminated because it includes unsupported types of potentials
	ElviraParserSVTest.class,
	ElviraWriterTest.class,
	ElviraWriterCanonicModelsTest.class,
	ElviraNameAndTitleTest.class,
	Cromosomopatia2Test.class,
	SuperValueTest.class
})

/** @author marias
 * @vesion 1.0 */
public class ElviraTests {

}
