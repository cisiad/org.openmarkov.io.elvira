package org.openmarkov.io.elvira;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.model.network.ProbNet;

public class ElviraNameAndTitleTest {

	private static String testFile = "BNU2-NasoNet.elv";
	
	private ProbNet probNet;
	
	@Before
	/** Create a ElviraScanner and opens a file for tests */
	public void setUp() throws Exception {
		URL url = this.getClass().getClassLoader().getResource(testFile);
		probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
	}
	
	@Test
	public void testTitleAndName() throws NodeNotFoundException {
		assertNotNull(probNet.getVariable("Primary infiltrating tumor on nasopharyngeal anterior wall"));
	}

}