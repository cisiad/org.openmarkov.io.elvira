/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.io.elvira;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.State;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.canonical.ICIPotential;
import org.openmarkov.core.model.network.type.BayesianNetworkType;

/** @author marias
 * @vesion 1.0 */
public class ElviraParserTest {

	// Constants
	double maxError = 1E-5;
	
	// Attributes
	private static String testFile = "cataratas-escenarios-091123.elv";
	
	private ProbNet probNet;
	private ElviraParser elviraParser;
	
	@Before
	/** Create a ElviraScanner and opens a file for tests */
	public void setUp() throws Exception {
		URL url=this.getClass().getClassLoader().getResource(testFile);
		elviraParser = new ElviraParser();
		probNet = elviraParser.loadProbNet(url.getFile()).getProbNet();
	}
	
	// Unit tests
	@Test
	public void getConstraints() throws ConstraintViolationException {
		assertEquals(BayesianNetworkType.class, 
				probNet.getNetworkType().getClass());
	}
	
	@Test
	public void getGeneralInfo() throws Exception {
		String kindOfGraph = 
				(String)probNet.additionalProperties.get("KindOfGraph");
		assertNotNull(kindOfGraph);
		assertTrue(kindOfGraph.contentEquals("mixed"));
		Double version = Double.parseDouble(
				probNet.additionalProperties.get("Version"));
		assertEquals(1.0, version.doubleValue(), maxError);
		State[] defaultNodeStates = probNet.getDefaultStates();
		assertNotNull(defaultNodeStates);
		assertEquals(2, defaultNodeStates.length);
		assertTrue(defaultNodeStates[0].getName().contentEquals("ausente"));
		assertTrue(defaultNodeStates[1].getName().contentEquals("presente"));
	}
	
	@Test
	public void getNodes() throws Exception {
		assertEquals(65, probNet.getNumNodes()); // Test number of nodes
		// Test some attributes of one random node
		String nameOfVariable = "v_ganancia_deslu"; 
		Node node = probNet.getNode(nameOfVariable);
		assertNotNull(node);
		Variable variable = node.getVariable();
		assertEquals(7, variable.getNumStates());
		assertEquals(3, variable.getStateIndex("igual"));
		Integer posX = (int)node.getCoordinateX();
		assertNotNull(posX);
		assertEquals(429, posX.intValue());
		// Test variables
		List<Variable> variables = probNet.getVariables();
		assertEquals(65, variables.size());
	}
	
	@Test
	public void getLinks() throws Exception {
		// Test children of one random node
		String nameOfVariable = "retinopatia_diabetic"; 
		Node node = probNet.getNode(nameOfVariable);
		List<Node> children = node.getChildren();
		assertEquals(6, children.size());
	}

	@Test
	public void getPotentials() throws Exception {
	    List<Potential> potentials = probNet.getPotentials();
		assertEquals(65, potentials.size());
		// Test a canonical potential
		// Gets variable mechaVitrea
		Variable mechaVitreaVariable = probNet.getVariable("mecha_vitrea");
		assertNotNull(mechaVitreaVariable);
		// Gets potentials corresponding to variable mechaVitrea
		List<Potential> mechaVitreaPotentials = 
			probNet.getPotentials(mechaVitreaVariable);
		assertEquals(5, mechaVitreaPotentials.size());
		ICIPotential mechaVitreaPotential = 
			(ICIPotential) mechaVitreaPotentials.get(0);
		// Test sub-potentials
		Variable miopiaMagnaVariable = probNet.getVariable("miopia_magna");
		assertNotNull(miopiaMagnaVariable);
        // Test conversion to OpenMarkov potential
        // Test ordination of variables
        assertEquals (miopiaMagnaVariable, mechaVitreaPotential.getVariable (1));
        assertEquals (mechaVitreaVariable, mechaVitreaPotential.getVariable (0));
        // Test potential table
        assertEquals (1.0, mechaVitreaPotential.getNoisyParameters (miopiaMagnaVariable)[0],
                      maxError);
        assertEquals (0.0, mechaVitreaPotential.getNoisyParameters (miopiaMagnaVariable)[1],
                      maxError);
        assertEquals (0.95, mechaVitreaPotential.getNoisyParameters (miopiaMagnaVariable)[2],
                      maxError);
        assertEquals (0.05, mechaVitreaPotential.getNoisyParameters (miopiaMagnaVariable)[3],
                      maxError);
	}

    // Integration tests
    /**
     * Load a probNet with a non binary variable.
     * @throws Exception
     */
    @Test
    public void loadANodeThreeState ()
        throws Exception
    {
        String testFile = "UnNodoTresEstados.elv";
        URL url = this.getClass ().getClassLoader ().getResource (testFile);
        elviraParser = new ElviraParser ();
        ProbNet unNodoTresEstados = elviraParser.loadProbNet (url.getFile ()).getProbNet ();
        Node uniqueNode = unNodoTresEstados.getNode ("Tres_estados");
        Variable uniqueVariable = uniqueNode.getVariable ();
        assertEquals (3, uniqueVariable.getNumStates ());
    }
	
}
