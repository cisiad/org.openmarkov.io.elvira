package org.openmarkov.io.elvira;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.List;

import org.junit.Test;
import org.openmarkov.core.exception.ParserException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.SumPotential;

/** @author marias */
public class SuperValueTest {

	private ProbNet probNet;
	
	@Test
	public void test1() throws NodeNotFoundException {
		try {
			URL url = this.getClass().getClassLoader().getResource("IDE4-decide-test.elv");
			probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (ParserException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		Node node = probNet.getNode("Global utility", NodeType.UTILITY);
		assertNotNull(node);
		List<Potential> potentials = node.getPotentials();
		assertEquals(1, potentials.size());
	    assertTrue(potentials.get (0) instanceof SumPotential);
//		Potential potential = potentials.get(0);
		
	}

	@Test
	public void test2() {
		try {
			URL url = this.getClass().getClassLoader().getResource("IDU2-rodilla.elv");
			probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (ParserException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		assertNotNull(probNet);
	}


	@Test
	public void test3() {
		try {
			URL url = this.getClass().getClassLoader().getResource("IDU2-rodilla-ce.elv");
			probNet = new ElviraParser().loadProbNet(url.getFile()).getProbNet();
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (ParserException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		assertNotNull(probNet);
	}
}


